def measure(n = 1)
  time_before = Time.now
  n.times { |block| yield }
  time_after = Time.now
  (time_after - time_before) / n
end
